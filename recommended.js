'use strict';

const eslintCommentsConfig = require('./eslint-comments-config.js');
const jestConfig = require('./jest-config.js');
const jsdocConfig = require('./jsdoc-config.js');
const nodeConfig = require('./node-config.js');
const playwrightConfig = require('./playwright-config.js');
const promiseConfig = require('./promise-config.js');
const unicornConfigs = require('./unicorn-configs.js');
const baseConfigs = require('./base-configs.js');
const prettierConfig = require('eslint-config-prettier');

module.exports = [
    eslintCommentsConfig,
    jestConfig,
    jsdocConfig,
    nodeConfig,
    playwrightConfig,
    promiseConfig,
    ...unicornConfigs,
    ...baseConfigs,
    { name: 'prettier (all files)', ...prettierConfig }
];
