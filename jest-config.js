'use strict';

const jestPlugin = require('eslint-plugin-jest');
const globals = require('globals');

module.exports = {
    files: [
        '**/__tests__/**/*.{js,mjs,cjs}',
        '**/?(*.)+(spec|test).{js,mjs,cjs}'
    ],
    languageOptions: {
        globals: {
            ...globals.jest
        }
    },
    name: 'jest (test files)',
    plugins: { jest: jestPlugin },
    rules: {
        'jest/consistent-test-it': [
            'error',
            { fn: 'it', withinDescribe: 'it' }
        ],
        'jest/expect-expect': 'error',
        'jest/max-expects': ['error', { max: 5 }],
        'jest/max-nested-describe': 'error',
        'jest/no-alias-methods': 'error',
        'jest/no-commented-out-tests': 'error',
        'jest/no-conditional-expect': 'error',
        'jest/no-conditional-in-test': 'error',
        'jest/no-deprecated-functions': 'error',
        'jest/no-disabled-tests': 'error',
        'jest/no-done-callback': 'error',
        'jest/no-duplicate-hooks': 'error',
        'jest/no-export': 'error',
        'jest/no-focused-tests': 'error',
        'jest/no-hooks': 'off',
        'jest/no-identical-title': 'error',
        'jest/no-interpolation-in-snapshots': 'error',
        'jest/no-jasmine-globals': 'error',
        'jest/no-large-snapshots': 'off',
        'jest/no-mocks-import': 'error',
        'jest/no-restricted-matchers': [
            'error',
            {
                'not.toBeFalsy': 'Avoid `not.toBeFalsy`',
                'not.toBeTruthy': 'Avoid `not.toBeTruthy`',
                'rejects.not.toBeFalsy': 'Avoid `rejects.not.toBeFalsy`',
                'rejects.not.toBeTruthy': 'Avoid `rejects.not.toBeTruthy`',
                'rejects.toBeFalsy': 'Avoid `rejects.toBeFalsy`',
                'rejects.toBeTruthy': 'Avoid `rejects.toBeTruthy`',
                'resolves.not.toBeFalsy': 'Avoid `resolves.not.toBeFalsy`',
                'resolves.not.toBeTruthy': 'Avoid `resolves.not.toBeTruthy`',
                'resolves.toBeFalsy': 'Avoid `resolves.toBeFalsy`',
                'resolves.toBeTruthy': 'Avoid `resolves.toBeTruthy`',
                toBeFalsy: 'Avoid `toBeFalsy`',
                toBeTruthy: 'Avoid `toBeTruthy`'
            }
        ],
        'jest/no-standalone-expect': 'error',
        'jest/no-test-prefixes': 'error',
        'jest/no-test-return-statement': 'error',
        'jest/no-untyped-mock-factory': 'off',
        'jest/prefer-called-with': 'error',
        'jest/prefer-comparison-matcher': 'error',
        'jest/prefer-each': 'error',
        'jest/prefer-equality-matcher': 'error',
        'jest/prefer-expect-assertions': 'error',
        'jest/prefer-expect-resolves': 'error',
        'jest/prefer-hooks-in-order': 'error',
        'jest/prefer-hooks-on-top': 'error',
        'jest/prefer-importing-jest-globals': 'off',
        'jest/prefer-jest-mocked': 'error',
        'jest/prefer-lowercase-title': 'error',
        'jest/prefer-mock-promise-shorthand': 'error',
        'jest/prefer-snapshot-hint': 'error',
        'jest/prefer-spy-on': 'error',
        'jest/prefer-strict-equal': 'error',
        'jest/prefer-to-be': 'error',
        'jest/prefer-to-contain': 'error',
        'jest/prefer-to-have-length': 'error',
        'jest/prefer-todo': 'error',
        'jest/require-hook': 'error',
        'jest/require-to-throw-message': 'error',
        'jest/require-top-level-describe': 'error',
        'jest/valid-describe-callback': 'error',
        'jest/valid-expect': 'error',
        'jest/valid-expect-in-promise': 'error',
        'jest/valid-title': 'error'
    }
};
