'use strict';

const eslintCommentsConfig = require('./eslint-comments-config.js');
const jsdocConfig = require('./jsdoc-config.js');
const nodeConfig = require('./node-config.js');
const playwrightConfig = require('./playwright-config.js');
const promiseConfig = require('./promise-config.js');
const unicornConfigs = require('./unicorn-configs.js');
const vitestConfig = require('./vitest-config.js');
const baseConfigs = require('./base-configs.js');
const esmConfig = require('./esm-config.js');
const prettierConfig = require('eslint-config-prettier');

module.exports = [
    eslintCommentsConfig,
    jsdocConfig,
    nodeConfig,
    playwrightConfig,
    promiseConfig,
    ...unicornConfigs,
    vitestConfig,
    ...baseConfigs,
    esmConfig,
    prettierConfig
];
