'use strict';

const recommendedConfig = require('./recommended.js');

module.exports = [
    ...recommendedConfig,
    {
        ignores: [
            '.eslint-config-inspector/**',
            '.vscode/**',
            'Archive/**',
            'node_modules/**',
            'coverage/**'
        ],
        name: 'ignores'
    }
];
