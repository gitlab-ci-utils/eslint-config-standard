'use strict';

const unicornPlugin = require('eslint-plugin-unicorn');

module.exports = {
    files: ['**/*.{js,mjs}'],
    languageOptions: {
        sourceType: 'module'
    },
    name: 'esm (all files)',
    plugins: { unicorn: unicornPlugin },
    rules: {
        'unicorn/prefer-export-from': 'error',
        'unicorn/prefer-module': 'error',
        'unicorn/prefer-node-protocol': 'error',
        'unicorn/prefer-top-level-await': 'error'
    }
};
