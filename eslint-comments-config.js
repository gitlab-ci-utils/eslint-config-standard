'use strict';

const eslintCommentsPlugin = require('@eslint-community/eslint-plugin-eslint-comments');

module.exports = {
    files: ['**/*.{js,mjs,cjs}'],
    name: 'eslint-comments (all files)',
    plugins: { comments: eslintCommentsPlugin },
    rules: {
        'comments/disable-enable-pair': ['error', { allowWholeFile: true }],
        'comments/no-aggregating-enable': 'error',
        'comments/no-duplicate-disable': 'error',
        'comments/no-unlimited-disable': 'error',
        'comments/no-unused-disable': 'error',
        'comments/no-unused-enable': 'error',
        'comments/no-use': [
            'error',
            {
                allow: [
                    'eslint-disable',
                    'eslint-disable-line',
                    'eslint-disable-next-line',
                    'eslint-enable'
                ]
            }
        ],
        'comments/require-description': 'error'
    }
};
