'use strict';

const playwrightPlugin = require('eslint-plugin-playwright');
const globals = require('globals');

const maxExpects = 5;

module.exports = {
    files: ['**/*.pwtest.{js,mjs,cjs}'],
    languageOptions: {
        globals: {
            ...globals['shared-node-browser']
        }
    },
    name: 'playwright (pwtest files)',
    plugins: { playwright: playwrightPlugin },
    rules: {
        'playwright/expect-expect': 'error',
        'playwright/max-expects': ['error', { max: maxExpects }],
        'playwright/max-nested-describe': 'error',
        'playwright/missing-playwright-await': 'error',
        'playwright/no-commented-out-tests': 'error',
        'playwright/no-conditional-expect': 'error',
        'playwright/no-conditional-in-test': 'error',
        'playwright/no-duplicate-hooks': 'error',
        'playwright/no-element-handle': 'error',
        'playwright/no-eval': 'error',
        'playwright/no-focused-test': 'error',
        'playwright/no-force-option': 'error',
        'playwright/no-get-by-title': 'error',
        'playwright/no-hooks': 'off',
        'playwright/no-nested-step': 'error',
        'playwright/no-networkidle': 'error',
        'playwright/no-nth-methods': 'error',
        'playwright/no-page-pause': 'error',
        'playwright/no-raw-locators': 'error',
        'playwright/no-restricted-matchers': 'off',
        'playwright/no-skipped-test': 'error',
        'playwright/no-slowed-test': 'error',
        'playwright/no-standalone-expect': 'error',
        'playwright/no-unsafe-references': 'error',
        'playwright/no-useless-await': 'error',
        'playwright/no-useless-not': 'error',
        'playwright/no-wait-for-selector': 'error',
        'playwright/no-wait-for-timeout': 'error',
        'playwright/prefer-comparison-matcher': 'error',
        'playwright/prefer-equality-matcher': 'error',
        'playwright/prefer-hooks-in-order': 'error',
        'playwright/prefer-hooks-on-top': 'error',
        'playwright/prefer-locator': 'error',
        'playwright/prefer-lowercase-title': 'error',
        'playwright/prefer-native-locators': 'error',
        'playwright/prefer-strict-equal': 'error',
        'playwright/prefer-to-be': 'error',
        'playwright/prefer-to-contain': 'error',
        'playwright/prefer-to-have-count': 'error',
        'playwright/prefer-to-have-length': 'error',
        'playwright/prefer-web-first-assertions': 'error',
        'playwright/require-hook': 'error',
        'playwright/require-soft-assertions': 'off',
        'playwright/require-to-throw-message': 'error',
        'playwright/require-top-level-describe': 'error',
        'playwright/valid-describe-callback': 'error',
        'playwright/valid-expect': 'error',
        'playwright/valid-expect-in-promise': 'error',
        'playwright/valid-title': 'error'
    }
};
