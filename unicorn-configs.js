'use strict';

const unicornPlugin = require('eslint-plugin-unicorn');

module.exports = [
    {
        files: ['**/*.{js,mjs,cjs}'],
        name: 'unicorn (all files)',
        plugins: { unicorn: unicornPlugin },
        rules: {
            'unicorn/better-regex': 'error',
            'unicorn/catch-error-name': 'error', // Recommended
            'unicorn/consistent-destructuring': 'error',
            'unicorn/consistent-empty-array-spread': 'error', // Recommended
            'unicorn/consistent-existence-index-check': 'error', // Recommended
            'unicorn/consistent-function-scoping': 'error', // Recommended
            'unicorn/custom-error-definition': 'error',
            'unicorn/empty-brace-spaces': 'off', // Recommended, Prettier
            'unicorn/error-message': 'error', // Recommended
            'unicorn/escape-case': 'error', // Recommended
            'unicorn/expiring-todo-comments': 'off', // Recommended
            'unicorn/explicit-length-check': 'error', // Recommended
            'unicorn/filename-case': ['error', { case: 'kebabCase' }], // Recommended
            'unicorn/import-style': 'error', // Recommended
            'unicorn/new-for-builtins': 'error', // Recommended
            'unicorn/no-abusive-eslint-disable': 'error', // Recommended
            'unicorn/no-anonymous-default-export': 'error', // Recommended
            'unicorn/no-array-callback-reference': 'error', // Recommended
            'unicorn/no-array-for-each': 'error', // Recommended
            'unicorn/no-array-method-this-argument': 'error', // Recommended
            'unicorn/no-array-push-push': 'error', // Recommended
            'unicorn/no-array-reduce': 'error', // Recommended
            'unicorn/no-await-expression-member': 'error', // Recommended
            'unicorn/no-await-in-promise-methods': 'error', // Recommended
            'unicorn/no-console-spaces': 'error', // Recommended
            'unicorn/no-document-cookie': 'error', // Recommended
            'unicorn/no-empty-file': 'error', // Recommended
            'unicorn/no-for-loop': 'error', // Recommended
            'unicorn/no-hex-escape': 'error', // Recommended
            'unicorn/no-instanceof-array': 'error', // Recommended
            'unicorn/no-invalid-fetch-options': 'error', // Recommended
            'unicorn/no-invalid-remove-event-listener': 'error', // Recommended
            'unicorn/no-keyword-prefix': 'off', // Recommended
            'unicorn/no-length-as-slice-end': 'error', // Recommended
            'unicorn/no-lonely-if': 'error', // Recommended
            'unicorn/no-magic-array-flat-depth': 'error', // Recommended
            'unicorn/no-negated-condition': 'error', // Recommended
            'unicorn/no-negation-in-equality-check': 'error', // Recommended
            'unicorn/no-nested-ternary': 'error', // Recommended, Prettier
            'unicorn/no-new-array': 'error', // Recommended
            'unicorn/no-new-buffer': 'error', // Recommended
            'unicorn/no-null': 'error', // Recommended
            'unicorn/no-object-as-default-parameter': 'error', // Recommended
            'unicorn/no-process-exit': 'error', // Recommended
            'unicorn/no-single-promise-in-promise-methods': 'error', // Recommended
            'unicorn/no-static-only-class': 'error', // Recommended
            'unicorn/no-thenable': 'error', // Recommended
            'unicorn/no-this-assignment': 'error', // Recommended
            'unicorn/no-typeof-undefined': 'error', // Recommended
            'unicorn/no-unnecessary-await': 'error', // Recommended
            'unicorn/no-unnecessary-polyfills': 'error', // Recommended
            'unicorn/no-unreadable-array-destructuring': 'error', // Recommended
            'unicorn/no-unreadable-iife': 'error', // Recommended
            'unicorn/no-unused-properties': 'error',
            'unicorn/no-useless-fallback-in-spread': 'error', // Recommended
            'unicorn/no-useless-length-check': 'error', // Recommended
            'unicorn/no-useless-promise-resolve-reject': 'error', // Recommended
            'unicorn/no-useless-spread': 'error', // Recommended
            'unicorn/no-useless-switch-case': 'error', // Recommended
            'unicorn/no-useless-undefined': 'error', // Recommended
            'unicorn/no-zero-fractions': 'error', // Recommended
            'unicorn/number-literal-case': 'error', // Prettier // Recommended
            'unicorn/numeric-separators-style': 'error', // Recommended
            'unicorn/prefer-add-event-listener': 'error', // Recommended
            'unicorn/prefer-array-find': 'error', // Recommended
            'unicorn/prefer-array-flat': 'error', // Recommended
            'unicorn/prefer-array-flat-map': 'error', // Recommended
            'unicorn/prefer-array-index-of': 'error', // Recommended
            'unicorn/prefer-array-some': 'error', // Recommended
            'unicorn/prefer-at': 'error', // Recommended
            'unicorn/prefer-blob-reading-methods': 'error', // Recommended
            'unicorn/prefer-code-point': 'error', // Recommended
            'unicorn/prefer-date-now': 'error', // Recommended
            'unicorn/prefer-default-parameters': 'error', // Recommended
            'unicorn/prefer-dom-node-append': 'error', // Recommended
            'unicorn/prefer-dom-node-dataset': 'error', // Recommended
            'unicorn/prefer-dom-node-remove': 'error', // Recommended
            'unicorn/prefer-dom-node-text-content': 'error', // Recommended
            'unicorn/prefer-event-target': 'error', // Recommended
            'unicorn/prefer-export-from': 'error', // Recommended
            'unicorn/prefer-global-this': 'error', // Recommended
            'unicorn/prefer-includes': 'error', // Recommended
            'unicorn/prefer-json-parse-buffer': 'off',
            'unicorn/prefer-keyboard-event-key': 'error', // Recommended
            'unicorn/prefer-logical-operator-over-ternary': 'error', // Recommended
            'unicorn/prefer-math-min-max': 'error', // Recommended
            'unicorn/prefer-math-trunc': 'off', // Recommended
            'unicorn/prefer-modern-dom-apis': 'error', // Recommended
            'unicorn/prefer-modern-math-apis': 'error', // Recommended
            'unicorn/prefer-module': 'off', // Recommended
            'unicorn/prefer-native-coercion-functions': 'error', // Recommended
            'unicorn/prefer-negative-index': 'error', // Recommended
            'unicorn/prefer-node-protocol': 'error', // Recommended
            'unicorn/prefer-number-properties': 'error', // Recommended
            'unicorn/prefer-object-from-entries': 'off', // Recommended
            'unicorn/prefer-optional-catch-binding': 'error', // Recommended
            'unicorn/prefer-prototype-methods': 'error', // Recommended
            'unicorn/prefer-query-selector': 'error', // Recommended
            'unicorn/prefer-reflect-apply': 'error', // Recommended
            'unicorn/prefer-regexp-test': 'error', // Recommended
            'unicorn/prefer-set-has': 'error', // Recommended
            'unicorn/prefer-set-size': 'error', // Recommended
            'unicorn/prefer-spread': 'error', // Recommended
            'unicorn/prefer-string-raw': 'error', // Recommended
            'unicorn/prefer-string-replace-all': 'error', // Recommended
            'unicorn/prefer-string-slice': 'error', // Recommended
            'unicorn/prefer-string-starts-ends-with': 'error', // Recommended
            'unicorn/prefer-string-trim-start-end': 'error', // Recommended
            'unicorn/prefer-structured-clone': 'error', // Recommended
            'unicorn/prefer-switch': 'error', // Recommended
            'unicorn/prefer-ternary': 'error', // Recommended
            'unicorn/prefer-top-level-await': 'off', // Recommended
            'unicorn/prefer-type-error': 'error', // Recommended
            'unicorn/prevent-abbreviations': [
                'error',
                {
                    allowList: {
                        args: true,
                        env: true,
                        i: true,
                        pkg: true
                    }
                }
            ], // Recommended
            'unicorn/relative-url-style': 'error', // Recommended
            'unicorn/require-array-join-separator': 'error', // Recommended
            'unicorn/require-number-to-fixed-digits-argument': 'error', // Recommended
            'unicorn/require-post-message-target-origin': 'off',
            'unicorn/string-content': 'off',
            'unicorn/switch-case-braces': ['error', 'always'], // Recommended
            'unicorn/template-indent': 'error', // Recommended
            'unicorn/text-encoding-identifier-case': 'error', // Recommended
            'unicorn/throw-new-error': 'error' // Recommended
        }
    },
    {
        files: [
            // Patterns for jest-config, vitest-config
            '**/__tests__/**/*.{js,mjs,cjs}',
            '**/?(*.)+(spec|test).{js,mjs,cjs}',
            // Pattern for playwright-config
            '**/*.pwtest.{js,mjs,cjs}'
        ],
        name: 'unicorn (test files)',
        rules: {
            // Tests utility functions are more readable when they are
            // nested with the tests
            'unicorn/consistent-function-scoping': 'off'
        }
    }
];
