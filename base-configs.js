/* eslint-disable max-lines -- required given number of rules */
'use strict';

const complexityThresholdLow = 5;
const maxFunctionLinesThreshold = 30;
const maxLinesThreshold = 300;
const maxParametersThreshold = 4;
const maxStatementsThreshold = 50;
const nestedThreshold = 5;

const configs = [
    {
        files: ['**/*.{js,mjs,cjs}'],
        languageOptions: {
            sourceType: 'commonjs'
        },
        linterOptions: {
            reportUnusedDisableDirectives: 'error'
        },
        name: 'eslint (all files)',
        rules: {
            // Possible problems
            'array-callback-return': 'error',
            'constructor-super': 'error', // Recommended
            'for-direction': 'error', // Recommended
            'getter-return': 'error', // Recommended
            'no-async-promise-executor': 'error', // Recommended
            'no-await-in-loop': 'error',
            'no-class-assign': 'error', // Recommended
            'no-compare-neg-zero': 'error', // Recommended
            'no-cond-assign': 'error', // Recommended
            'no-const-assign': 'error', // Recommended
            'no-constant-binary-expression': 'error', // Recommended
            'no-constant-condition': 'error', // Recommended
            'no-constructor-return': 'error',
            'no-control-regex': 'error', // Recommended
            'no-debugger': 'error', // Recommended
            'no-dupe-args': 'error', // Recommended
            'no-dupe-class-members': 'error', // Recommended
            'no-dupe-else-if': 'error', // Recommended
            'no-dupe-keys': 'error', // Recommended
            'no-duplicate-case': 'error', // Recommended
            'no-duplicate-imports': 'error',
            'no-empty-character-class': 'error', // Recommended
            'no-empty-pattern': 'error', // Recommended
            'no-ex-assign': 'error', // Recommended
            'no-fallthrough': 'error', // Recommended
            'no-func-assign': 'error', // Recommended
            'no-import-assign': 'error', // Recommended
            'no-inner-declarations': 'error',
            'no-invalid-regexp': 'error', // Recommended
            'no-irregular-whitespace': ['error', { skipStrings: true }], // Recommended
            'no-loss-of-precision': 'error', // Recommended
            'no-misleading-character-class': 'error', // Recommended
            'no-new-native-nonconstructor': 'error', // Recommended
            'no-obj-calls': 'error', // Recommended
            'no-promise-executor-return': 'error',
            'no-prototype-builtins': 'error', // Recommended
            'no-self-assign': 'error', // Recommended
            'no-self-compare': 'error',
            'no-setter-return': 'error', // Recommended
            'no-sparse-arrays': 'error', // Recommended
            'no-template-curly-in-string': 'error',
            'no-this-before-super': 'error', // Recommended
            'no-undef': 'error', // Recommended
            'no-unexpected-multiline': 'error', // Recommended, Prettier
            'no-unmodified-loop-condition': 'error',
            'no-unreachable': 'error', // Recommended
            'no-unreachable-loop': 'error',
            'no-unsafe-finally': 'error', // Recommended
            'no-unsafe-negation': 'error', // Recommended
            'no-unsafe-optional-chaining': 'error', // Recommended
            'no-unused-private-class-members': 'error', // Recommended
            'no-unused-vars': ['error', { args: 'all' }], // Recommended
            'no-use-before-define': ['error', { functions: false }],
            'no-useless-backreference': 'error', // Recommended
            'require-atomic-updates': 'error',
            'use-isnan': 'error', // Recommended
            'valid-typeof': 'error', // Recommended

            // Suggestions
            // eslint-disable-next-line sort-keys -- match eslint docs
            'accessor-pairs': 'error',
            'arrow-body-style': ['error', 'as-needed'],
            'block-scoped-var': 'error',
            camelcase: 'error',
            'capitalized-comments': [
                'error',
                'always',
                { ignoreConsecutiveComments: true, ignorePattern: 'nosemgrep' }
            ],
            'class-methods-use-this': 'error',
            complexity: [
                'error',
                { max: complexityThresholdLow, variant: 'modified' }
            ],
            'consistent-return': 'error',
            // Disabled due to 'unicorn/no-this-assignment
            'consistent-this': 'off',
            curly: 'error', // Prettier
            'default-case': 'error',
            'default-case-last': 'error',
            'default-param-last': 'error',
            'dot-notation': ['error', { allowPattern: '^[a-z]+(_[a-z]+)+$' }],
            eqeqeq: ['error', 'always'],
            'func-name-matching': 'off',
            'func-names': 'off',
            'func-style': [
                'error',
                'declaration',
                { allowArrowFunctions: true }
            ],
            'grouped-accessor-pairs': ['error', 'getBeforeSet'],
            'guard-for-in': 'error',
            'id-denylist': 'off',
            'id-length': 'off',
            'id-match': 'off',
            'init-declarations': 'off',
            'logical-assignment-operators': ['error', 'always'],
            'max-classes-per-file': ['error', 1],
            'max-depth': ['error', { max: nestedThreshold }],
            'max-lines': ['error', maxLinesThreshold],
            'max-lines-per-function': [
                'error',
                {
                    max: maxFunctionLinesThreshold,
                    skipBlankLines: true,
                    skipComments: true
                }
            ],
            'max-nested-callbacks': ['error', { max: nestedThreshold }],
            'max-params': ['error', { max: maxParametersThreshold }],
            'max-statements': ['error', { max: maxStatementsThreshold }],
            'new-cap': 'error',
            'no-alert': 'error',
            'no-array-constructor': 'error',
            'no-bitwise': 'error',
            'no-caller': 'error',
            'no-case-declarations': 'error', // Recommended
            'no-console': 'off',
            'no-continue': 'error',
            'no-delete-var': 'error', // Recommended
            'no-div-regex': 'error',
            'no-else-return': 'error',
            'no-empty': 'error', // Recommended
            'no-empty-function': ['error', { allow: ['arrowFunctions'] }],
            'no-empty-static-block': 'error', // Recommended
            'no-eq-null': 'error',
            'no-eval': 'error',
            'no-extend-native': 'error',
            'no-extra-bind': 'error',
            'no-extra-boolean-cast': 'error', // Recommended
            'no-extra-label': 'error',
            'no-global-assign': 'error', // Recommended
            'no-implicit-coercion': 'error',
            'no-implicit-globals': 'off',
            'no-implied-eval': 'error',
            'no-inline-comments': 'off',
            'no-invalid-this': 'error',
            'no-iterator': 'error',
            'no-label-var': 'error',
            'no-labels': 'error',
            'no-lone-blocks': 'error',
            'no-lonely-if': 'error',
            'no-loop-func': 'error',
            'no-magic-numbers': ['error', { ignore: [-1, 0, 1] }],
            'no-multi-assign': 'error',
            'no-multi-str': 'error',
            // Disabled in favor of unicorn/no-negated-condition
            'no-negated-condition': 'off',
            // Disabled in favor of unicorn/no-nested-ternary
            'no-nested-ternary': 'off',
            'no-new': 'error',
            'no-new-func': 'error',
            'no-new-wrappers': 'error',
            'no-nonoctal-decimal-escape': 'error', // Recommended
            'no-object-constructor': 'error',
            'no-octal': 'error', // Recommended
            'no-octal-escape': 'error',
            'no-param-reassign': ['error', { props: false }],
            'no-plusplus': 'off',
            'no-proto': 'error',
            'no-redeclare': 'error', // Recommended
            'no-regex-spaces': 'error', // Recommended
            'no-restricted-exports': 'off',
            'no-restricted-globals': 'off',
            'no-restricted-imports': 'off',
            'no-restricted-properties': 'off',
            'no-restricted-syntax': 'off',
            'no-return-assign': 'error',
            'no-script-url': 'error',
            'no-sequences': ['error', { allowInParentheses: false }],
            'no-shadow': 'error',
            'no-shadow-restricted-names': 'error', // Recommended
            'no-ternary': 'off',
            'no-throw-literal': 'error',
            'no-undef-init': 'error',
            // Disabled, too many valid positives
            'no-undefined': 'off',
            'no-underscore-dangle': 'error',
            'no-unneeded-ternary': 'error',
            'no-unused-expressions': [
                'error',
                { allowShortCircuit: true, allowTernary: true }
            ],
            'no-unused-labels': 'error', // Recommended
            'no-useless-call': 'error',
            'no-useless-catch': 'error', // Recommended
            'no-useless-computed-key': 'error',
            'no-useless-concat': 'error',
            'no-useless-constructor': 'error',
            'no-useless-escape': 'error', // Recommended
            'no-useless-rename': 'error',
            'no-useless-return': 'error',
            'no-var': 'error',
            'no-void': 'error',
            'no-warning-comments': [
                'error',
                { terms: ['TODO', 'FIXME', 'HACK', 'XXX', 'BUG'] }
            ],
            'no-with': 'error', // Recommended
            'object-shorthand': 'error',
            'one-var': 'off',
            'operator-assignment': 'error',
            'prefer-arrow-callback': 'error',
            'prefer-const': 'error',
            'prefer-destructuring': ['error', { array: true, object: true }],
            'prefer-exponentiation-operator': 'error',
            'prefer-named-capture-group': 'error',
            'prefer-numeric-literals': 'error',
            'prefer-object-has-own': 'error',
            'prefer-object-spread': ['error'],
            'prefer-promise-reject-errors': 'error',
            'prefer-regex-literals': 'error',
            'prefer-rest-params': 'error',
            'prefer-spread': 'error',
            'prefer-template': 'error',
            radix: ['error', 'as-needed'],
            'require-await': 'error',
            'require-unicode-regexp': 'off',
            'require-yield': 'error', // Recommended
            'sort-imports': ['error', { allowSeparatedGroups: true }],
            'sort-keys': [
                'error',
                'asc',
                { caseSensitive: true, natural: true }
            ],
            'sort-vars': 'error',
            // 'spaced-comment': 'off',
            strict: 'error',
            'symbol-description': 'error',
            // Disabled due to no-var
            'vars-on-top': 'off',
            yoda: 'error',

            // Layout & Formatting
            // eslint-disable-next-line sort-keys -- match eslint docs
            'line-comment-position': 'off',
            'unicode-bom': 'off' // Prettier

            // Deprecated
            // 'array-bracket-newline': 'off', // Prettier
            // 'array-bracket-spacing': 'off', // Prettier
            // 'array-element-newline': 'off', // Prettier
            // 'arrow-parens': ['error', 'always'], // Prettier
            // 'arrow-spacing': 'error', // Prettier
            // 'block-spacing': 'error', // Prettier
            // 'brace-style': ['error', 'stroustrup'], // Prettier
            // 'comma-dangle': ['error', 'never'], // Prettier
            // 'comma-spacing': ['error', { after: true, before: false }], // Prettier
            // 'comma-style': 'off', // Prettier
            // 'computed-property-spacing': ['error', 'never'], // Prettier
            // 'dot-location': ['error', 'property'], // Prettier
            // 'eol-last': ['error', 'always'], // Prettier
            // 'func-call-spacing': ['error', 'never'], // Prettier
            // 'function-call-argument-newline': 'off', // Prettier
            // 'function-paren-newline': 'off', // Prettier
            // 'generator-star-spacing': 'off', // Prettier
            // 'implicit-arrow-linebreak': 'off', // Prettier
            // indent: ['error', 4, { SwitchCase: 1 }], // Prettier
            // 'jsx-quotes': 'off', // Prettier
            // 'key-spacing': 'error', // Prettier
            // 'keyword-spacing': ['error', { after: true, before: true }], // Prettier
            // 'linebreak-style': ['error', 'unix'], // Prettier
            // 'lines-around-comment': 'off', // Prettier
            // 'lines-between-class-members': 'off',
            // 'max-len': 'off', // Prettier
            // 'max-statements-per-line': ['error', { max: 1 }],
            // 'multiline-comment-style': ['error', 'separate-lines'],
            // 'multiline-ternary': 'off', // Prettier
            // 'new-parens': 'error', // Prettier
            // 'newline-per-chained-call': 'off', // Prettier
            // 'no-confusing-arrow': 'error', // Prettier
            // 'no-extra-parens': 'off', // Prettier
            // 'no-extra-semi': 'error', // Prettier
            // 'no-floating-decimal': 'error', // Prettier
            // 'no-mixed-operators': 'error', // Prettier
            // 'no-mixed-spaces-and-tabs': 'error', // Prettier
            // 'no-multi-spaces': 'error', // Prettier
            // 'no-multiple-empty-lines': 'off', // Prettier
            // 'no-new-object': 'off', // replaced by no-object-constructor
            // 'no-new-symbol': 'off', // replaced by no-new-native-nonconstructor
            // 'no-return-await': 'off',
            // 'no-tabs': 'off', // Prettier
            // 'no-trailing-spaces': 'error', // Prettier
            // 'no-whitespace-before-property': 'error', // Prettier
            // 'nonblock-statement-body-position': 'off', // Prettier
            // 'object-curly-newline': 'off', // Prettier
            // 'object-curly-spacing': ['error', 'always'], // Prettier
            // 'object-property-newline': 'off', // Prettier
            // 'one-var-declaration-per-line': 'off', // Prettier
            // 'operator-linebreak': 'off', // Prettier
            // 'padded-blocks': ['error', 'never'], // Prettier
            // 'padding-line-between-statements': 'off',
            // 'quote-props': 'off', // Prettier
            // quotes: ['error', 'single'], // Prettier
            // 'rest-spread-spacing': 'error', // Prettier
            // semi: ['error', 'always'], // Prettier
            // 'semi-spacing': 'error', // Prettier
            // 'semi-style': ['error', 'last'], // Prettier
            // 'space-before-blocks': ['error', 'always'], // Prettier
            // 'space-before-function-paren': [
            //     'error',
            //     {
            //         anonymous: 'always',
            //         asyncArrow: 'always',
            //         named: 'never'
            //     }
            // ], // Prettier
            // 'space-in-parens': ['error', 'never'], // Prettier
            // 'space-infix-ops': 'off', // Prettier
            // 'space-unary-ops': 'off', // Prettier
            // 'switch-colon-spacing': 'off', // Prettier
            // 'template-curly-spacing': 'error', // Prettier
            // 'template-tag-spacing': 'off', // Prettier
            // 'wrap-iife': 'error', // Prettier
            // 'wrap-regex': 'off', // Prettier
            // 'yield-star-spacing': 'off' // Prettier
        }
    },
    {
        files: [
            // Patterns for jest-config, vitest-config
            '**/__tests__/**/*.{js,mjs,cjs}',
            '**/?(*.)+(spec|test).{js,mjs,cjs}',
            // Pattern for playwright-config
            '**/*.pwtest.{js,mjs,cjs}'
        ],
        name: 'eslint (test and pwtest files)',
        rules: {
            // Assuming 1 test file per module, it can exceed limit
            'max-lines': 'off',
            // Describes are functions and frequently exceed limit
            'max-lines-per-function': 'off',
            // Tests frequently use indices and counts
            'no-magic-numbers': 'off',
            // Undefined is a common value to test no value
            'no-undefined': 'off',
            // Tests frequently check array elements, destructuring is
            // les intuitive
            'prefer-destructuring': 'off',
            // Regexes in tests frequently check for match patterns, but
            // groups are not captured
            'prefer-named-capture-group': 'off',
            // Async tests frequently return promises, which the test
            // runner awaits implicitly
            'require-await': 'off'
        }
    }
];

if (process.env.ENABLE_ESLINT_CORE_RULE_DUPLICATES === 'true') {
    /* eslint-disable-next-line node/global-require -- given the experimental
       nature, only require when enabled */
    const { builtinRules } = require('eslint/use-at-your-own-risk');
    const complexity = builtinRules.get('complexity');

    const complexityThresholdHigh = 15;
    const coreDuplicateConfig = {
        files: ['**/*.{js,mjs,cjs}'],
        name: 'eslint core duplicates (all files)',
        plugins: {
            // Add plugin that duplicates the required rules
            core: { rules: { complexity } }
        },
        rules: {
            'core/complexity': [
                'error',
                { max: complexityThresholdHigh, variant: 'modified' }
            ]
        }
    };

    configs.push(coreDuplicateConfig);
}

module.exports = configs;
