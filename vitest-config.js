'use strict';

const vitestPlugin = require('@vitest/eslint-plugin');

module.exports = {
    files: ['**/__tests__/**/*.{js,mjs}', '**/?(*.)+(spec|test).{js,mjs}'],
    name: 'vitest (test files)',
    plugins: { vitest: vitestPlugin },
    rules: {
        'vitest/consistent-test-filename': [
            'error',
            {
                allTestPattern: String.raw`.*\.test\.(js|mjs)$`,
                pattern: String.raw`.*\.test\.(js|mjs)$`
            }
        ],
        'vitest/consistent-test-it': [
            'error',
            { fn: 'it', withinDescribe: 'it' }
        ],
        'vitest/expect-expect': 'error', // Recommended
        'vitest/max-expects': ['error', { max: 5 }],
        'vitest/max-nested-describe': ['error', { max: 3 }],
        'vitest/no-alias-methods': 'error',
        'vitest/no-commented-out-tests': 'error', // Recommended
        'vitest/no-conditional-expect': 'error',
        'vitest/no-conditional-in-test': 'error',
        'vitest/no-conditional-tests': 'error',
        'vitest/no-disabled-tests': 'error',
        'vitest/no-done-callback': 'error',
        'vitest/no-duplicate-hooks': 'error',
        'vitest/no-focused-tests': 'error',
        'vitest/no-hooks': 'off',
        'vitest/no-identical-title': 'error', // Recommended
        'vitest/no-interpolation-in-snapshots': 'error',
        'vitest/no-large-snapshots': 'off',
        'vitest/no-mocks-import': 'error',
        'vitest/no-restricted-matchers': 'off',
        'vitest/no-restricted-vi-methods': 'off',
        'vitest/no-standalone-expect': 'error',
        'vitest/no-test-prefixes': 'error',
        'vitest/no-test-return-statement': 'error',
        'vitest/padding-around-after-all-blocks': 'off',
        'vitest/padding-around-after-each-blocks': 'off',
        'vitest/padding-around-all': 'off',
        'vitest/padding-around-before-all-blocks': 'off',
        'vitest/padding-around-before-each-blocks': 'off',
        'vitest/padding-around-describe-blocks': 'off',
        'vitest/padding-around-expect-groups': 'off',
        'vitest/padding-around-test-blocks': 'off',
        'vitest/prefer-called-with': 'error',
        'vitest/prefer-comparison-matcher': 'error',
        'vitest/prefer-each': 'error',
        'vitest/prefer-equality-matcher': 'error',
        'vitest/prefer-expect-resolves': 'error',
        'vitest/prefer-hooks-in-order': 'error',
        'vitest/prefer-hooks-on-top': 'error',
        'vitest/prefer-lowercase-title': 'error',
        'vitest/prefer-mock-promise-shorthand': 'error',
        'vitest/prefer-snapshot-hint': 'error',
        'vitest/prefer-spy-on': 'error',
        'vitest/prefer-strict-equal': 'error',
        'vitest/prefer-to-be': 'error', // Recommended
        'vitest/prefer-to-be-falsy': 'off',
        'vitest/prefer-to-be-object': 'error',
        'vitest/prefer-to-be-truthy': 'off',
        'vitest/prefer-to-contain': 'error',
        'vitest/prefer-to-have-length': 'error',
        'vitest/prefer-todo': 'error',
        'vitest/require-hook': 'error',
        'vitest/require-to-throw-message': 'error',
        'vitest/require-top-level-describe': 'error',
        'vitest/valid-describe-callback': 'error', // Recommended
        'vitest/valid-expect': 'error', // Recommended
        'vitest/valid-title': 'error' // Recommended
    }
};
