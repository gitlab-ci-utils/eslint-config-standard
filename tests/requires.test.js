'use strict';

const { ESLint } = require('eslint');

const globals = require('globals');

/**
 * Lints code using the specified configuration and checks results for
 * errors and warnings. Will throw if the configuration is invalid.
 *
 * @param {string[]} config  The eslint configuration file to be tested.
 * @param {string}   code    The code to test against the configuration file.
 * @param {object}   options Options for lintText.
 * @param {number}   errors  The number of expected eslint errors.
 */
const testConfig = async (config, code, options = {}, errors = 0) => {
    if (!Array.isArray(config)) {
        throw new TypeError('Config must be an array');
    }

    expect.assertions(4);
    const cli = new ESLint({
        baseConfig: [
            {
                languageOptions: {
                    globals: {
                        ...globals.node
                    },
                    sourceType: 'commonjs'
                },
                linterOptions: {
                    reportUnusedDisableDirectives: 'error'
                }
            }
        ],
        overrideConfig: config,
        overrideConfigFile: true
    });

    const results = await cli.lintText(code, options);

    // Per the docs (https://eslint.org/docs/developer-guide/nodejs-api#return-value-1)
    // this array should only ever have one entry, so fail if not
    expect(results).toHaveLength(1);
    expect(results[0].errorCount).toBe(errors);
    expect(results[0].warningCount).toBe(0);
    expect(results[0].fatalErrorCount).toBe(0);
};

const baseConfigs = require('../base-configs.js');
const eslintCommentsConfig = require('../eslint-comments-config.js');
const jestConfig = require('../jest-config.js');
const jsdocConfig = require('../jsdoc-config.js');
const nodeConfig = require('../node-config.js');
const playwrightConfig = require('../playwright-config.js');
const promiseConfig = require('../promise-config.js');
const unicornConfigs = require('../unicorn-configs.js');
const vitestConfig = require('../vitest-config.js');
const recommendedConfigs = require('../recommended.js');
const recommendedEsmConfigs = require('../recommended-esm.js');
const esmConfig = require('../esm-config.js');

const defaultLintOptions = { filePath: 'base-configs.js' };
const testLintOptions = { filePath: './tests/index.test.js' };
const playwrightLintOptions = { filePath: './tests-ui/index.pwtest.js' };

/* eslint-disable sort-keys -- more readable */
const testCases = [
    {
        name: 'default',
        config: baseConfigs,
        nominalCode: "'use strict';\nconst foo = 0;\nconsole.log(foo);\n",
        // Code should fail rule no-undef
        errorCode: "'use strict';\nconsole.log(foo);\n",
        options: defaultLintOptions
    },
    {
        name: 'eslint-comments',
        config: [eslintCommentsConfig],
        nominalCode: `
            // eslint-disable
            const foo = 0;`,
        // Code should fail rule comments/require-description,
        // comments/no-unlimited-disable, and
        // unused eslint-disable directive
        errorCode: `
            const foo = 0;

            /* eslint-disable comments/no-unlimited-disable */
            const bar = 0;`,
        errorCount: 3,
        options: defaultLintOptions
    },
    {
        name: 'jest',
        config: [jestConfig],
        nominalCode: `
            describe('tests', function() {
                it('should be a test', function() {
                    expect.hasAssertions();
                    expect(true).toBe(true);
                });
            });`,
        // Code should fail rule jest/no-focused-tests
        errorCode: `
            describe('tests', function() {
                it.only('write some tests', function() {
                    expect.assertions(1);
                    expect(true).toBe(true);
                });
            });`,
        options: testLintOptions
    },
    {
        name: 'jsdoc',
        config: [jsdocConfig],
        nominalCode: `
            /**
             * Tests jsdoc rules.
             */
            describe('tests', function(foo) {
                it.todo('write some tests');
            });`,
        // Code should fail rule jsdoc/require-returns
        errorCode: `
            /**
             * Tests jsdoc rules.
             */
            function foo() {
                return true;
            };`,
        options: defaultLintOptions
    },
    {
        name: 'node',
        config: [nodeConfig],
        nominalCode: `const fullPath1 = path.join(__dirname, "foo.js");`,
        // Code should fail rule node/no-path-concat
        errorCode: `const fullPath1 = __dirname + "/foo.js";`,
        options: defaultLintOptions
    },
    {
        name: 'playwright',
        config: [playwrightConfig],
        nominalCode: `
            test.describe('tests', () => {
                test('playwright test', async ({ page }) => {
                    await page.goto(reportUrl);
                    expect(true).toBe(true);
                });
            });`,
        // Code should fail rule playwright/require-top-level-describe
        errorCode: `
            test('playwright test', async ({ page }) => {
                await page.goto(reportUrl);
                expect(true).toBe(true);
            });`,
        options: playwrightLintOptions
    },
    {
        name: 'promise',
        config: [promiseConfig],
        nominalCode: `
            function foo() {
                return Promise.resolve(true);
            }`,
        // Code should fail rule promise/no-new-statics
        errorCode: `
            new Promise.resolve(true);`,
        options: defaultLintOptions
    },
    {
        name: 'unicorn',
        config: unicornConfigs,
        nominalCode: 'const value = 30;',
        // Code should fail rule unicorn/numeric-separators-style.
        errorCode: 'const value = 30000;',
        options: defaultLintOptions
    },
    {
        name: 'vitest',
        config: [vitestConfig],
        nominalCode: `
            describe('tests', function() {
                it('should be a test', function() {
                    expect.hasAssertions();
                    expect(true).toBe(true);
                });
            });`,
        // Code should fail rule vitest/no-focused-tests
        errorCode: `
            describe('tests', function() {
                it.only('write some tests', function() {
                    expect.assertions(1);
                    expect(true).toBe(true);
                });
            });`,
        options: testLintOptions
    },
    {
        name: 'recommended',
        config: recommendedConfigs,
        nominalCode: `
            'use strict';
            /**
             * This is a function.
             *
             * @param   {string} a This is a parameter.
             * @returns {string}   Returns "foo" if input is truthy, otherwise "bar".
             */
            function foo(a) {
                return a ? 'foo' : 'bar';
            }
            module.exports.foo = foo;`,
        // Code should fail rules jest/require-hook, jest/prefer-to-contain,
        // jsdoc/require-jsdoc, n/no-missing-require,
        // unicorn/prefer-ternary, no-else-return
        errorCode: `
            'use strict';
            require('notamodule');
            function foo(a) {
                expect(a.includes(1)).toBe(true);
                if (a) {
                    return true;
                } else {
                    return false;
                }
            }
            module.exports.foo = foo;`,
        errorCount: 6,
        options: testLintOptions
    },
    {
        name: 'recommended-esm',
        config: recommendedEsmConfigs,
        nominalCode: `
            /**
             * This is a function.
             *
             * @param   {string} a This is a parameter.
             * @returns {string}   Returns "foo" if input is truthy, otherwise "bar".
             */
            function foo(a) {
                return a ? 'foo' : 'bar';
            }
            export { foo };`,
        // Code should fail rules jsdoc/require-jsdoc, node/no-missing-import
        // unicorn/prefer-ternary, vitest/prefer-to-contain, no-else-return,
        // no-undef
        errorCode: `
            import 'notamodule';
            function foo(a) {
                expect(a.includes(1)).toBe(true);
                if (a) {
                    return true;
                } else {
                    return false;
                }
            }
            export { foo };`,
        errorCount: 6,
        options: testLintOptions
    },
    {
        name: 'esm',
        config: [esmConfig],
        nominalCode: 'import notamodule from "notamodule"',
        // Code should fail rule unicorn/prefer-module.
        errorCode: 'require("notamodule");',
        options: defaultLintOptions
    }
];
/* eslint-enable sort-keys -- more readable */

describe('test nominal config', () => {
    it.each(testCases)(
        'should lint code using $name config and find no errors',
        async ({ config, nominalCode, options }) => {
            expect.hasAssertions();
            await testConfig(config, nominalCode, options);
        }
    );
});

describe('test failing config', () => {
    it.each(testCases)(
        'should lint code using $name config and find the appropriate errors',
        async ({ config, errorCode, errorCount = 1, options }) => {
            expect.hasAssertions();
            await testConfig(config, errorCode, options, errorCount);
        }
    );
});

describe('test file overrides', () => {
    // Extract only test cases with config overrides
    const overrideConfigs = new Set(['jest', 'playwright']);
    const overrideTestCases = testCases.filter(({ name }) =>
        overrideConfigs.has(name)
    );

    it.each(overrideTestCases)(
        'should lint non-test code with errors using $name config and find no errors',
        async ({ config, errorCode }) => {
            expect.hasAssertions();
            await testConfig(config, errorCode, defaultLintOptions);
        }
    );
});
