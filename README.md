# @aarongoldenthal/eslint-config-standard

## Summary

Custom ESLint configuration for all projects. Includes flat-config formatted
configurations compatible with ESLint v9+ for the following:

| Plugin Name                                       | Config filename                                                                  | Rule Prefix  |
| ------------------------------------------------- | -------------------------------------------------------------------------------- | ------------ |
| `eslint`                                          | [`base-configs.js`](./base-configs.js)                                           | none, `core` |
| `@eslint-community/eslint-plugin-eslint-comments` | [`eslint-comments-config.js`](./eslint-comments-config.js)                       | `comments`   |
| `eslint-plugin-jest`                              | [`jest-config.js`](./jest-config.js)                                             | `jest`       |
| `eslint-plugin-jsdoc`                             | [`jsdoc-config.js`](./jsdoc-config.js)                                           | `jsdoc`      |
| `eslint-plugin-n`                                 | [`node-config.js`](./node-config.js)                                             | `node`       |
| `eslint-plugin-playwright`                        | [`playwright-config.js`](./playwright-config.js)                                 | `playwright` |
| `eslint-plugin-promise`                           | [`promise-config.js`](./promise-config.js)                                       | `promise`    |
| `eslint-plugin-unicorn`                           | [`unicorn-configs.js`](./unicorn-configs.js), [`esm-config.js`](./esm-config.js) | `unicorn`    |
| `eslint-plugin-vitest`                            | [`vitest-config.js`](./vitest-config.js)                                         | `vitest`     |

As flat configs, the package defines all required plugins/configurations as
`dependencies`. Since flat config allows flexibility in the rule prefixes (that
is, they don't have to match the plugin name), the rules prefixes are adapted
in some cases to be more intuitive or readable. Since they may be combined, and
flat config doesn't allow nested arrays of rules, file names that are singular
export a single config object (for example `jsdoc-config.js`), and file names
that are plural export an array of config objects (for example `base-configs.js`).

The `core` prefix has only one rule, `core/complexity`, that has a higher threshold.
This provides a secondary check for cases where the lower threshold in the `complexity`
rule is disabled, which otherwise allows unbounded complexity. Since re-use of core
rules is an experimental capability, this must be enabled with environment variable
`ENABLE_ESLINT_CORE_RULE_DUPLICATES=true`.

Most rule configurations are applicable to files matching `'**/*.{js,mjs,cjs}'`. The
following configurations are exceptions and are applicable to files as noted:

- `base-configs.js` includes a config that disables some rules for test files
  matching any of the following test patterns (for example `max-lines`,
  `max-lines-per-function`).
- `jest-config` applies rules to files matching
  `'**/__tests__/**/*.{js,mjs,cjs}'` or `'**/?(*.)+(spec|test).{js,mjs,cjs}'`.
- `vitest-config` applies rules to files matching
  `'**/__tests__/**/*.{js,mjs}'` or `'**/?(*.)+(spec|test).{js,mjs}'`.
- `playwright-config`: applies rules to files matching `'**/*.pwtest.{js,mjs,cjs}'`,
  which differentiates them from Jest/Vitest test files.
- `unicorn-configs.js` includes a config that disables some rules for test files
  (matching any of the following test patterns).

With ESLint v9 the majority of formatting rules are deprecated and removed from
`base-configs`, but the `eslint-config-prettier` package is included and can be
added to the config if `prettier` is also being used to ensure it takes priority for
formatting.

There is also an `esm-config` included with rule overrides for projects using
ES modules instead of Common JS modules.

### Usage

There is a `recommended` configuration with all plugin configurations enabled
except `esm-config` and `vitest-config` (it does include `jest-config`). To
configure `eslint.config.js` with this configuration:

```js
const recommendedConfig = require('@aarongoldenthal/eslint-config-standard/recommended');

module.exports = [
  ...recommendedConfig,
  {
    ignores: ['.vscode/**', 'archive/**', 'node_modules/**', 'coverage/**'],
    name: 'ignores'
  }
];
```

Note the optional `ignores` config that can be added last to ignore certain
directories.

There is also a `recommended-esm` configuration that's the same as the
`recommended` config, but includes the `vitest-config` instead of the
`jest-config`, and also the `esm-config`. It can be configured with:

```js
import recommendedConfig from '@aarongoldenthal/eslint-config-standard/recommended-esm.js';

export default [
  ...recommendedConfig,
  {
    ignores: ['.vscode/**', 'archive/**', 'node_modules/**', 'coverage/**'],
    name: 'ignores'
  }
];
```

To configure `eslint.config.js` with individual plugins, see the
[`recommended`](./recommended.js) or
[`recommended-esm`](./recommended-esm.js) configurations as examples.

Notes:

- If used, the `base-configs` should be included after other configurations,
  except `esm-config` and `prettier`, so those settings take precedence.
- The `jest-config` and `vitest-config` have the same file applicability, so
  only one should be used.
- If used, the `esm-config` should be configured after all functional rules
  to ensure the overridden settings take precedence.
- If used, the `prettier` config should be included last to take priority in
  disabling the applicable rules from all other configurations.
