# Changelog

## v34.0.0 (2025-01-23)

### Changed

- BREAKING: Updated `eslint` peer dependency to v9.18.0 or higher.
- BREAKING: Updated to `eslint-plugin-playwright@2.2.0` and added
  [`no-slowed-test`](https://github.com/playwright-community/eslint-plugin-playwright/blob/main/docs/rules/no-slowed-test.md)
  rule to `playwright-config` as error.

### Fixed

- Updated to latest dependencies: `@vitest/eslint-plugin@1.1.25`,
  `eslint-config-prettier@10.0.1`, `eslint-plugin-jest@28.11.0`,
  `eslint-plugin-jsdoc@50.6.2`, `eslint-plugin-promise@7.2.1`.

## v33.0.2 (2024-12-20)

### Fixed

- Updated to latest dependencies: `@vitest/eslint-plugin@1.1.20`,
  `eslint-plugin-jest@28.10.0`, `eslint-plugin-jsdoc@50.6.1`,
  `eslint-plugin-n@17.15.1`, `eslint-plugin-promise@7.2.1`,
  `globals@15.14.0`.

## v33.0.1 (2024-11-19)

### Fixed

- Updated to latest dependencies: `@vitest/eslint-plugin@1.1.10`,
  `eslint-plugin-jest@28.9.0`, `eslint-plugin-jsdoc@50.5.0`,
  `eslint-plugin-n@17.13.2`, `eslint-plugin-playwright@2.1.0`,
  `eslint-plugin-unicorn@56.0.1`.

## v33.0.0 (2024-10-31)

### Changed

- BREAKING: Updated to `eslint-plugin-playwright@2.0.0` and add new rules as
  errors:
  [`prefer-locator`](https://github.com/playwright-community/eslint-plugin-playwright/blob/main/docs/rules/prefer-locator.md),
  [`prefer-native-locators`](https://github.com/playwright-community/eslint-plugin-playwright/blob/main/docs/rules/prefer-native-locators.md).
- BREAKING: Removed `eslint-plugin-sonarjs` and the associated
  `sonarjs-config.js` after many issues with compatibility, including still
  not being compatible with `eslint` v9 (and with no identified plan to achieve
  compatibility even though v8 is officially deprecated), as well as the
  addition of numerous rules that duplicate `eslint` core rules, creating
  additional maintenance overhead. (#130)
  - Any inline rule disables will be reported as unused disable directives and
    will need to be removed.
  - Any direct inclusion of the `sonarjs-config.js` config will need to be
    removed.
  - Both `recommended*.js` configs have been updated to remove the config.
- BREAKING: With the removal of `sonarjs-config.js`, which had the
  `cognitive-complexity` rules with low and high thresholds, updated
  `base-configs.js` with low and high thresholds for `complexity`. (#132)
  - The `complexity` rule was updated to the low threshold of 5,
    and updated to use [`variant: modified`](https://eslint.org/docs/latest/rules/complexity#variant),
    which is closer to the `cognitive-complexity` implementation.
  - The `core/complexity` rule was added with the high threshold of 15, also
    using `variant: modified`. Since re-use of core rules is an experimental
    capability, this must be enabled with environment variable
    `ENABLE_ESLINT_CORE_RULE_DUPLICATES=true`.

### Fixed

- Updated to latest dependencies:
  `@eslint-community/eslint-plugin-eslint-comments@4.4.1`,
  `eslint-plugin-jsdoc@50.4.3`, `eslint-plugin-n@17.12.0`.

### Miscellaneous

- Updated CI pipelines to use GitLab pages new
  [parallel deployments](https://docs.gitlab.com/ee/user/project/pages/index.html#parallel-deployments)
  feature to publish `config-inspector` site for MRs. (#131)

## v32.0.0 (2024-10-09)

### Changed

- BREAKING: Updated to `eslint-plugin-unicorn@56.0.0` and add new rules as
  errors:
  [`prefer-global-this`](https://github.com/sindresorhus/eslint-plugin-unicorn/blob/main/docs/rules/prefer-global-this.md),
  [`prefer-math-min-max`](https://github.com/sindresorhus/eslint-plugin-unicorn/blob/main/docs/rules/prefer-math-min-max.md),
  [`consistent-existence-index-check`](https://github.com/sindresorhus/eslint-plugin-unicorn/blob/main/docs/rules/consistent-existence-index-check.md).

### Fixed

- Updated to latest dependencies: `@vitest/eslint-plugin@1.1.7`,
  `eslint-plugin-jsdoc@50.3.1`, `eslint-plugin-n@17.11.1`.

## v31.1.0 (2024-09-24)

### Changed

- Moved from `eslint-plugin-vitest`, no longer maintained, to fork
  `@vitest/eslint-plugin@1.1.4`. The rules are still referenced as `vitest`,
  so not BREAKING. (#129)
  - New formatting rules in
    [v1.1.0](https://github.com/vitest-dev/eslint-plugin-vitest/releases/tag/v1.1.0)
    are all disabled.

### Miscellaneous

- Updated `yarn` CI tests to install `peerDependencies` for
  `@vitest/eslint-plugin`.
- Updated to latest dependencies: `@eslint-community/eslint-plugin-eslint-comments@4.4.0`,
  `eslint-plugin-jest@28.8.3`, `eslint-plugin-jsdoc@50.2.4`,
  `eslint-plugin-n@17.10.3`.

## v31.0.1 (2024-08-10)

### Fixed

- Updated to latest dependencies: `eslint-plugin-jest@28.8.0`,
  `eslint-plugin-jsdoc@50.0.0`, `eslint-plugin-n@17.10.2`,
  `eslint-plugin-promise@7.1.0`, `globals@15.9.0`.

## v31.0.0 (2024-07-29)

### Changed

- BREAKING: Updated to `eslint-plugin-unicorn@55.0.0` and added rule
  [`no-length-as-slice-end`](https://github.com/sindresorhus/eslint-plugin-unicorn/blob/main/docs/rules/no-length-as-slice-end.md)
  as `error`.
- BREAKING: Updated to `eslint-plugin-jsdoc@48.9.2` and added rule
  [`check-template-names`](https://github.com/gajus/eslint-plugin-jsdoc/blob/main/docs/rules/check-template-names.md)
  as `error`.

### Fixed

- Updated to latest dependencies: `eslint-plugin-n@17.10.1`,
  `eslint-plugin-promise@7.0.0`, `eslint-plugin-sonarjs@1.0.4`.

## v30.0.0 (2024-07-12)

### Changed

- BREAKING: Updated to `eslint-plugin-jsdoc@48.7.0` and added new rule
  [`require-template`](https://github.com/gajus/eslint-plugin-jsdoc/blob/main/docs/rules/.md)
  as an error.

### Fixed

- Updated to `eslint-plugin-promise@6.4.0`, fixing error in rule
  `promise/no-multiple-resolved`. (#125)

## v29.0.0 (2024-06-23)

### Changed

- BREAKING: Updated `jest-config` to add rule
  [`prefer-jest-mocked`](https://github.com/jest-community/eslint-plugin-jest/blob/main/docs/rules/prefer-jest-mocked.md)
  as `error` (from `eslint-plugin-jest@28.6.0`). (#126)
- BREAKING: Updated to `eslint-plugin-unicorn@54.0.0` with the following
  breaking changes:
  - Added rule
    [`no-negation-in-equality-check`](https://github.com/sindresorhus/eslint-plugin-unicorn/blob/main/docs/rules/no-negation-in-equality-check.md)
    as `error`.
  - Update rule `prefer-array-find` option
    [`checkFromLast`](https://github.com/sindresorhus/eslint-plugin-unicorn/blob/main/docs/rules/prefer-array-find.md#checkfromlast)
    value to `true`.

### Fixed

- Updated to latest dependencies: `eslint-plugin-jest@28.6.0`,
  `eslint-plugin-jsdoc@48.2.15`, `eslint-plugin-n@17.9.0`.

### Miscellaneous

- Fixed flat config examples in README.

## v28.0.0 (2024-06-02)

### Changed

- BREAKING: Updated all configs to [flat config](https://eslint.org/blog/2022/08/new-config-system-part-2/)
  and require ESLint v9+. Config files have been renamed such that files that
  are singular (for example `jest-config.js`) export a single config object,
  but those that are plural (for example `unicorn-configs.js`) export an array
  of config objects. See the documentation for updated usage information.
  (#113)
  - Moved all plugins from `peerDependencies` to `dependencies` since they are
    now required for flat config.
  - Most rule configs are now applicable to `*.{js,mjs,cjs}` file extensions.
    In some cases this dropped support for `*.jsx`, `*.ts`, and `*.tsx` files,
    but support for them was previously inconsistent.
    - The exceptions are `vitest-config` and `esm-config`, which are ESM only,
      so they do not support `*.cjs` files. Rule
      `vitest/consistent-test-filename` was updated to match this convention.
    - See the [README](./README.md) for additional details.
  - With flat config, rules references do not have to match the plugin name, so
    updated rule prefixes to be more usable. Any suppressions for the following
    rules will need to be updated.
    - Plugin `eslint-plugin-n` rules are now `node/` instead of `n/`.
    - Plugin `@eslint-community/eslint-comments` rules are now `comments/`
      instead of `@eslint-community/eslint-comments/`.
  - The default ESLint rules are now in config `base-configs.js`.
    - Added rule `spaced-comment` as an error.
    - The deprecated formatting rules have been removed.
    - Deprecated rules `no-new-object`, `no-new-symbol`, and `no-return-await`
      have been removed.
  - Renamed the `recommended-vitest` config to `recommended-esm.js` and added
    the `esm-config` rules.
- BREAKING: Updated to `eslint-plugin-unicorn@53.0.0` and added the following new rules as `error`s:
  [`consistent-empty-array-spread`](https://github.com/sindresorhus/eslint-plugin-unicorn/blob/main/docs/rules/consistent-empty-array-spread.md),
  [`prefer-string-raw`](https://github.com/sindresorhus/eslint-plugin-unicorn/blob/main/docs/rules/prefer-string-raw.md),
  [`no-invalid-fetch-options`](https://github.com/sindresorhus/eslint-plugin-unicorn/blob/main/docs/rules/no-invalid-fetch-options.md),
  [`no-magic-array-flat-depth`](https://github.com/sindresorhus/eslint-plugin-unicorn/blob/main/docs/rules/no-magic-array-flat-depth.md),
  [`prefer-structured-clone`](https://github.com/sindresorhus/eslint-plugin-unicorn/blob/main/docs/rules/prefer-structured-clone.md).
  Also added the following rules as error from `eslint-plugin-unicorn@52.0.0`:
  [`no-await-in-promise-methods`](https://github.com/sindresorhus/eslint-plugin-unicorn/blob/main/docs/rules/no-await-in-promise-methods.md),[`no-single-promise-in-promise-methods`](https://github.com/sindresorhus/eslint-plugin-unicorn/blob/main/docs/rules/no-single-promise-in-promise-methods.md),
  [`no-anonymous-default-export`](https://github.com/sindresorhus/eslint-plugin-unicorn/blob/main/docs/rules/no-anonymous-default-export.md). (#122, #124)
- BREAKING: Updated to `eslint-plugin-n@17.7.0`, which renamed rule `shebang` to `hashbang`.
- BREAKING: Added new rule `sonarjs2/cognitive-complexity` to `sonarjs-config`
  that has a higher `cognitive-complexity` threshold of 15. This provides a
  secondary check if the lower threshold of 5 in the
  `sonarjs/cognitive-complexity` rule is disabled, which previously allowed
  unbounded complexity. (#84)
- BREAKING: Deprecated support for Node 21 (end-of-life 2024-06-01) and added
  support for Node 22 (released 2024-04-25). Compatible with all current and
  LTS releases (`^18.12.0 || ^20.9.0 || >=22.0.0`). (#123)

### Fixed

- Updated to latest dependencies: `@eslint-community/eslint-plugin-eslint-comments@4.3.0`,
  `eslint-plugin-jest@28.5.0`, `eslint-plugin-jsdoc@48.2.7`,
  `eslint-plugin-playwright@1.6.2`, `eslint-plugin-promise@6.2.0`,
  `eslint-plugin-sonarjs@1.0.3`, `eslint-plugin-unicorn@53.0.0`,
  `eslint-plugin-vitest@0.5.4`.

### Miscellaneous

- Updated CI pipeline with `eslint-config-inspector` job to use
  [`@eslint/config-inspector`](https://github.com/eslint/config-inspector) to
  visualize flat configs.
- Updated Renovate config to v1.1.0.

## v27.0.1 (2024-03-30)

### Fixed

- Updated to latest peer dependencies: `eslint-plugin-jsdoc@48.2.2`,
  `eslint-plugin-playwright@1.5.4`, `eslint-plugin-sonarjs@0.25.0`,
  `eslint-plugin-vitest@0.4.0`.

## v27.0.0 (2024-03-08)

### Changed

- BREAKING: Updated to `eslint-plugin-playwright@1.5.2` and added the following
  new rules as `error`s:
  [`require-to-throw-message`](https://github.com/playwright-community/eslint-plugin-playwright/blob/main/docs/rules/require-to-throw-message.md),
  [`valid-expect-in-promise`](https://github.com/playwright-community/eslint-plugin-playwright/blob/main/docs/rules/valid-expect-in-promise.md),
  [`valid-describe-callback`](https://github.com/playwright-community/eslint-plugin-playwright/blob/main/docs/rules/valid-describe-callback.md). (#121)

### Fixed

- Updated to latest peer dependencies: `eslint-plugin-jsdoc@48.2.1`, and
  `eslint-plugin-vitest@0.3.24`.

## v26.0.1 (2024-02-25)

### Fixed

- Updated to latest peer dependencies: `eslint-plugin-jsdoc@48.2.0`, and
  `eslint-plugin-playwright@1.3.1`. Resolved false positives from rule
  `playwright/require-hook`.

### Miscellaneous

- Reverted part of renovate config to properly manage `peerDependencies`. (#120)

## v26.0.0 (2024-02-19)

### Changed

- BREAKING: Updated to `eslint-plugin-playwright@1.3.0` and added the following
  new rules as `error`s.
  - From v1.3.0:
    [`prefer-comparison-matcher`](https://github.com/playwright-community/eslint-plugin-playwright/blob/main/docs/rules/prefer-comparison-matcher.md),
    [`prefer-equality-matcher`](https://github.com/playwright-community/eslint-plugin-playwright/blob/main/docs/rules/prefer-equality-matcher.md),
    [`require-hook`](https://github.com/playwright-community/eslint-plugin-playwright/blob/main/docs/rules/require-hook.md).
  - From v1.2.0:
    [`max-expects`](https://github.com/playwright-community/eslint-plugin-playwright/blob/main/docs/rules/max-expects.md),
    [`no-commented-out-tests`](https://github.com/playwright-community/eslint-plugin-playwright/blob/main/docs/rules/no-commented-out-tests.md),
    [`no-conditional-expect`](https://github.com/playwright-community/eslint-plugin-playwright/blob/main/docs/rules/no-conditional-expect.md),
    [`no-duplicate-hooks`](https://github.com/playwright-community/eslint-plugin-playwright/blob/main/docs/rules/no-duplicate-hooks.md),
    [`no-standalone-expect`](https://github.com/playwright-community/eslint-plugin-playwright/blob/main/docs/rules/no-standalone-expect.md),
    [`prefer-hooks-in-order`](https://github.com/playwright-community/eslint-plugin-playwright/blob/main/docs/rules/prefer-hooks-in-order.md),
    [`prefer-hooks-on-top`](https://github.com/playwright-community/eslint-plugin-playwright/blob/main/docs/rules/prefer-hooks-on-top.md)
  - From v1.1.0:
    [`no-unsafe-references`](https://github.com/playwright-community/eslint-plugin-playwright/blob/main/docs/rules/no-unsafe-references.md)
  - From v1.0.0:
    [`no-get-by-title`](https://github.com/playwright-community/eslint-plugin-playwright/blob/main/docs/rules/no-get-by-title.md)
  - From previous versions:
    [`valid-title`](https://github.com/playwright-community/eslint-plugin-playwright/blob/main/docs/rules/valid-title.md),
    [`no-wait-for-selector`](https://github.com/playwright-community/eslint-plugin-playwright/blob/main/docs/rules/no-wait-for-selector.md)
  - The new [`no-hooks`](https://github.com/playwright-community/eslint-plugin-playwright/blob/main/docs/rules/no-hooks.md)
    rule from v1.3.0 was added to the config as `off`.

### Fixed

- Updated to latest peer dependencies: `eslint-plugin-jest@27.9.0`,
  `eslint-plugin-jsdoc@48.1.0`, `eslint-plugin-n@16.6.2`,
  `eslint-plugin-sonarjs@0.24.0`, `eslint-plugin-unicorn@51.0.1`,
  and `eslint-plugin-vitest@0.3.22`.

### Miscellaneous

- Updated Renovate config to use use new [presets](https://gitlab.com/gitlab-ci-utils/renovate-config)
  project. (#118)

## v25.0.0 (2023-12-23)

### Changed

- BREAKING: Added rule `playwright/prefer-to-have-count` from
  `eslint-plugin-playwright@0.17.0`. (#114)
- BREAKING: Added rule `unicorn/no-unnecessary-polyfills` from
  `eslint-plugin-unicorn@50.0.0`. (#115)

### Fixed

- Updated to latest peer dependencies: `eslint-config-prettier@9.1.0`,
  `eslint-plugin-jsdoc@46.9.1`, `eslint-plugin-n@16.5.0`,
  `eslint-plugin-playwright@0.20.0`, `eslint-plugin-unicorn@50.0.1`,
  and `eslint-plugin-vitest@0.3.18`.

## v24.0.1 (2023-11-27)

### Fixed

- Updated to latest peer dependencies: `eslint-plugin-jest@27.6.0`,
  `eslint-plugin-jsdoc@46.9.0`, `eslint-plugin-n@16.3.1`,
  `eslint-plugin-playwright@0.18.0`, `eslint-plugin-sonarjs@0.23.0`,
  `eslint-plugin-unicorn@49.0.0`, and `eslint-plugin-vitest@0.3.10`.

## v24.0.0 (2023-10-02)

### Changed

- BREAKING: Deprecated support for Node 16 (end-of-life 2023-09-11).
  Compatible with all current and LTS releases (`^18.12.0 || >=20.0.0`). (#108)
- BREAKING: Disabled rule `no-return-await`, which was deprecated is
  `eslint@8.46.0`. (#110)
- BREAKING: Added rule `no-object-constructor` from `eslint@8.50.0`,
  deprecating rule `no-new-object`. (#111)
- BREAKING: Added rule `playwright/no-raw-locators`. (#106)
- BREAKING: Enabled rules `unicorn/import-style`,
  `unicorn/no-array-method-this-argument`, `unicorn/no-new-buffer`,
  `unicorn/prefer-export-from`, `unicorn/prefer-node-protocol`,
  `unicorn/prefer-prototype-methods`, `unicorn/prefer-reflect-apply`,
  and `unicorn/template-indent` as errors. (#105)

### Fixed

- Updated to latest peer dependencies:
  `@eslint-community/eslint-plugin-eslint-comments@4.1.0`,
  `eslint-config-prettier@9.0.0`, `eslint-plugin-jest@27.4.2`,
  `eslint-plugin-jsdoc@46.8.2`, `eslint-plugin-n@16.1.0`,
  `eslint-plugin-playwright@0.16.0`, `eslint-plugin-sonarjs@0.21.0`,
  and `eslint-plugin-vitest@0.3.1`.
  - Updated `sonarjs/no-duplicate-string` settings to match previous config
    with new settings.

### Miscellaneous

- Updated package scripts to new names. (#107)

## v23.1.1 (2023-07-26)

### Fixed

- Added missing `vitest-config` documentation to README.

## v23.1.0 (2023-07-26)

### Changed

- Added new config for `eslint-plugin-vitest`, which is included in a new
  `recommended-vitest` config, and can be added individually with
  `"extends": ["@aarongoldenthal/eslint-config-standard/vitest-config"]`.
  Note this config uses the same file overrides as `jest-config`, so only one
  should be used. (#104)

### Fixed

- Updated to latest plugins, including `eslint-plugin-unicorn@48.0.1`.

## v23.0.0 (2023-07-19)

### Changed

- BREAKING: Added new rules to `playwright-config` as errors: `expect-expect`,
  `no-nested-step`, `no-networkidle`, `no-nth-methods`, `no-useless-await`,
  `prefer-to-contain`, and `prefer-web-first-assertions`.
- BREAKING: Changed `jsdoc-config` rule `no-defaults` to error for any
  contexts. (#103)
- BREAKING: Added new rule `imports-as-dependencies` to `jsdoc-config`. (#102)
- BREAKING: Moved all `eslint` plugins and configurations to `peerDependencies`
  since all active Node versions now include npm versions that install then by
  default. This is only BREAKING if they are not installed (e.g. using `yarn` or
  `npm install --legacy-peer-deps`), and in that case they must all be added to
  the project. (#82)
  - Updated `yarn` tests to automatically install `peerDependencies`.
- BREAKING: Updated to `eslint-plugin-unicorn@48.0.0`, which removed rule
  `no-unsafe-regex` and chaged the behavior of several rules (see the
  [release notes](https://github.com/sindresorhus/eslint-plugin-unicorn/releases/tag/v48.0.0)).
  (!501)

### Fixed

- Updated to latest plugins, including `eslint-plugin-jest@27.2.3`,
  `eslint-plugin-jsdoc@46.4.4`, `eslint-plugin-n@16.0.1`,
  `eslint-plugin-playwright@0.15.3`, and `eslint-plugin-unicorn@48.0.0`.

### Miscellaneous

- Updated to latest `default` config for Renovate v36.

## v22.1.0 (2023-05-23)

### Changed

- Updated `max-params` threshold from 3 to 4 in default config. Note this
  could be breaking if this rule was previously disabled, but can be
  `--fix`ed. (#99)
- Disabled `no-undefined` rule in default config. Note this could be
  breaking if this rule was previously disabled, but can be `--fix`ed. (#100)
- Updated `no-warning-comments` rule in default config to disallow the terms
  'TODO', 'FIXME', 'HACK', 'XXX', 'BUG'. This is all values checked by the
  CodeClimate FIXME engine. This is not considered breaking because these are
  already checked in the GitLab Code Quality job. (#101)

### Fixed

- Updated to latest dependencies, including `eslint-plugin-jsdoc@44.2.5` and
  `eslint-plugin-n@16.0.0`.

## v22.0.0 (2023-05-14)

### Changed

- BREAKING: Deprecated support for Node 14 (end-of-life 2023-04-30) and 19
  (end-of-life 2023-06-01). Compatible with all current and LTS releases
  (`^16.13.0 || ^18.12.0 || >=20.0.0`). (#98)
- BREAKING: Updated default config (`eslint`) to specify all rules for clarity
  (`error` or `off`) and remove `eslint-recommended` config. There are 46 new
  rules added as errors, see the config for complete details. (#96)
- BREAKING: Added rule `no-blank-blocks` to `jsdoc-config` as en error, new in
  v43.1.0. (#95)
- BREAKING: Added four new rules as error to `unicorn-config`: (#94)
  - Added rule `prefer-blob-reading-methods`, new in v47.0.0.
  - Enabled rules `prefer-at`, `prefer-event-target`, and
    `prefer-string-replace-all`, which all require Node 16.
- Update default config (`eslint`) with standard test suppressions. (#92)
- Updated `jsdoc-config` to match previous layout with changes in
  `eslint-plugin-jsdoc` v42.0.0.

### Fixed

- Updated to latest dependencies, including `eslint-plugin-sonarjs@0.19.0`
  and `eslint-plugin-unicorn@47.0.0`.

## v21.0.0 (2023-04-12)

### Changed

- BREAKING: Added new rule `jsdoc/informative-docs` from `eslint-plugin-jsdoc`
  v41.0.0. (#91)
- BREAKING: Added new config for `@eslint-community/eslint-plugin-eslint-comments`
  (formerly `eslint-plugin-eslint-comments`), which is included in the
  `recommended` config, and can be added individually with
  `"extends": ["@aarongoldenthal/eslint-config-standard/eslint-comments-config"]`. (#44)

### Fixed

- Updated to latest dependencies, including `eslint-config-prettier@8.8.0`,
  `eslint-plugin-jsdoc@41.1.1`, `eslint-plugin-n@15.7.0`, and
  `eslint-plugin-sonarjs@0.19.0`.

## v20.1.0 (2023-03-14)

### Changed

- Added `esm-config` with overrides for projects using ES modules instead of
  Common JS modules. (#90)

### Fixed

- Updated to latest dependencies, including `eslint-config-prettier@8.7.0`,
  `eslint-plugin-jsdoc@40.0.2`, and `eslint-plugin-unicorn@46.0.0`

## v20.0.0 (2023-01-19)

### Changed

- BREAKING: Added new rules `playwright/prefer-to-be` and
  `playwright/prefer-strict-equal` from `eslint-plugin-playwright` v0.12.0. (#88)

### Fixed

- Updated to latest dependencies, including `eslint-plugin-jest@27.2.1`,
  `eslint-plugin-jsdoc@39.6.7`, `eslint-plugin-n@15.6.1`,
  `eslint-plugin-playwright@0.12.0`, and `eslint-plugin-sonarjs@0.18.0`.

## v19.0.2 (2023-01-03)

### Fixed

- Updated package to pin `dependencies` since they are tied to the rule
  configurations. (#86)
  - Added package unique `.npmpackagejsonlintrc.json` to allow pinned
    dependencies.
- Updated to latest dependencies, including `eslint-plugin-jest@27.2.0` and
  `eslint-config-prettier@8.6.0`.
  - Added rule `jest/no-untyped-mock-factory` as disabled since only applicable
    to TypeScript. (#85)

## v19.0.1 (2022-12-20)

### Fixed

- Added missing `recommended` config file to package. (#83)

## v19.0.0 (2022-12-20)

### Changed

- BREAKING: Added new rules `unicorn/no-negated-condition`, `unicorn/no-typeof-undefined`,
  and `unicorn/prefer-set-size` from `eslint-plugin-unicorn` v45.0.0. (#79)
- Added new config for `eslint-plugin-promise`, which can be added by
  `"extends": ["@aarongoldenthal/eslint-config-standard/promise-config"]`. (#80)
- Added new `recommended` config with all other configurations enabled. See the
  [README](README.md) for details. (#81)

### Fixed

- Updated to latest dependencies, including `eslint-plugin-jest@27.1.7`,
  `eslint-plugin-jsdoc@39.6.4`, and `eslint-plugin-n@15.6.0`.

## v18.0.0 (2022-11-12)

### Changed

- BREAKING: Added new `eslint` rules:
  - Rule `logical-assignment-operators` from `eslint` v8.24.0. (#75)
  - Rules `no-empty-static-block` and `no-new-native-nonconstructor` from `eslint` v8.27.0.
    This also required updating `env` to `es2022` and peerDependencies to
    `"eslint": "^8.27.0"`. (#78)
- BREAKING: Added new rules `unicorn/no-unnecessary-await` and `unicorn/switch-case-braces`
  from `eslint-plugin-unicorn` v44.0.0. (#77)
- BREAKING: Moved from `eslint-plugin-node` to
  [`eslint-plugin-n`](https://www.npmjs.com/package/eslint-plugin-n) as this is the currently
  maintained fork. The config remains name `node-config`, with no rule changes, but any rule
  disable statements will need to be update to reflect the new rule names
  (i.e. `n/rule` instead of `node/rule`). (#60)
- Added new config for `eslint-plugin-playwright`, which can be added by
  `"extends": ["@aarongoldenthal/eslint-config-standard/playwright-config"]`. (#74)

### Fixed

- Updated to latest dependencies, including `eslint-plugin-jest@27.1.5`,
  `eslint-plugin-jsdoc@39.6.2`, `eslint-plugin-sonarjs@0.16.0`, `eslint-plugin-unicorn@44.0.2`.

### Miscellaneous

- Updated pipeline to have long-running jobs use GitLab shared `medium` sized runners. (#76)

## v17.0.1 (2022-09-05)

### Fixed

- Updated `jsdoc-config` to add missing `typedef` to `check-indentation` and `check-line-alignment`
  to align columns (with `param`, `property`, `returns`, and `throws`). (#73)

## v17.0.0 (2022-09-04)

### Changed

- BREAKING: Updated `jest-config` for changes in `eslint-plugin-jest`:
  - Added rule `jest/prefer-each`, new in v26.9.0 (#70)
  - Removed `jest/no-jest-import` rule ([removed in v27.0.0](https://github.com/jest-community/eslint-plugin-jest/pull/1220)) (#71)
- BREAKING: Updated `jsdoc-config` to specify all rules for clarity (`error` or `off`). (#69)
  - Added the following as `errors`: `check-indentation`, `check-line-alignment`,
    `match-description`, `require-throws`, and `sort-tags`.
  - The `check-indentation` and `check-line-alignment` configs are set to align `param`,
    `property`, `returns`, and `throws`.

### Fixed

- Updated to latest dependencies, including `eslint-plugin-jest@27.0.1`.
- Updated `jest-config` rule `jest/no-restricted-matchers` to restrict all permutations of `toBeTruthy` and `toBeFalsy` after updates in `eslint-plugin-jest` v27.0.0. (#71)
- Updated `sonarjs-config` and `unicorn-config` to explicitly list all rules, even if `off`. (#72)

## v16.0.1 (2022-08-22)

### Fixed

- Fixed `jest-config` to only apply to test files, which eliminates false positives with some rules in non-test files. Uses the default Jest [`testMatch`](https://jestjs.io/docs/configuration#testmatch-arraystring) value for files. (#68)

## v16.0.0 (2022-08-21)

### Changed

- BREAKING: Added new rules in `eslint-plugin-jest` v26.6.0 ([`max-expects`](https://github.com/jest-community/eslint-plugin-jest/blob/main/docs/rules/max-expects.md) with default of 5) and v26.7.0 ([`prefer-mock-promise-shorthand`](https://github.com/jest-community/eslint-plugin-jest/blob/main/docs/rules/prefer-mock-promise-shorthand.md)) to `jest-config`. (#65, #66)
- BREAKING: Reviewed all current `eslint-plugin-jest` rules for inclusion in `jest-config` config. (#67)
  - Updated configuration to explicitly specify all rules rather than rely on `jest/recommended`.
  - Added rules [`no-test-return-statement`](https://github.com/jest-community/eslint-plugin-jest/blob/main/docs/rules/no-test-return-statement.md) and [`require-hook`](https://github.com/jest-community/eslint-plugin-jest/blob/main/docs/rules/require-hook.md).
- BREAKING: Added new rule from `eslint-plugin-unicorn` v43.0.0 ([`prefer-logical-operator-over-ternary`](https://github.com/sindresorhus/eslint-plugin-unicorn/blob/main/docs/rules/prefer-logical-operator-over-ternary.md)) to `unicorn-config`. (#64)

### Fixed

- Updated to latest dependencies, including `eslint-plugin-jest`, `eslint-plugin-jsdoc`, `eslint-plugin-sonarjs`, and `eslint-plugin-unicorn`.

## v15.0.0 (2022-06-12)

### Changed

- BREAKING: Added rule [`no-constant-binary-expression`](https://eslint.org/docs/rules/no-constant-binary-expression), which also required updating `peerDependency` to `"eslint@^8.14.0"`. (#61)
- BREAKING: Added rules [`jest/prefer-hooks-in-order`](https://github.com/jest-community/eslint-plugin-jest/blob/main/docs/rules/prefer-hooks-in-order.md) and [`jest/prefer-hooks-on-top`](https://github.com/jest-community/eslint-plugin-jest/blob/main/docs/rules/prefer-hooks-on-top.md). (#63)
- BREAKING: Deprecated support for Node 12 and 17 since end-of-life. Compatible with all current and LTS releases (`^14.15.0 || ^16.13.0 || >=18.0.0`).

### Fixed

- Fixed tests for `jest` v28 (no longer requires module name mapper)
- Updated to latest dependencies, including `eslint-plugin-jest` and `eslint-plugin-jsdoc`

### Miscellaneous

- Added test coverage threshold requirements (#62)

## v14.0.0 (2022-04-12)

### Changed

- BREAKING: Added back rule [`jest/prefer-snapshot-hint`](https://github.com/jest-community/eslint-plugin-jest/blob/main/docs/rules/prefer-snapshot-hint.md) with false positives now fixed. (#56)
- Added `eslint-config-prettier` to allow disabling all formatting rules in favor of `prettier` (#58)

### Fixed

- Updated to latest dependencies, including `eslint-plugin-jest` and `eslint-plugin-jsdoc`

### Miscellaneous

- Added `prettier` and disabled `eslint` formatting rules. (#59)

## v13.0.0 (2022-04-04)

### Changed

- BREAKING: Added new rules in `eslint-plugin-unicorn` v42.0.0 (`unicorn/no-useless-switch-case`, `unicorn/prefer-modern-math-apis`, `unicorn/no-unreadable-iife`, `unicorn/prefer-native-coercion-functions`) (#57)
- BREAKING: Added new rule from `eslint-plugin-unicorn` v41.0.0 (`unicorn/text-encoding-identifier-case`) (#54)
- BREAKING: Added previously disabled rules from `eslint-plugin-unicorn` (`unicorn/prefer-optional-catch-binding`, `unicorn/relative-url-style`) (#57)
- BREAKING: Added `eslint` rule `arrow-parens` (#55)

### Fixed

- Updated to latest dependencies, including:
  - Latest versions of `eslint-plugin-jest`, `eslint-plugin-jsdoc`, `eslint-plugin-sonarjs`, and `eslint-plugin-unicorn`
  - Resolved CVE-2021-44906 (dev-only)

## v12.0.2 (2022-02-20)

### Fixed

- Updated documentation to specify include order (#52)
- Disable `unicorn/prefer-string-replace-all` since it requires Node >= 15.0.0 (#53)
- Update to latest dependencies, including `eslint-plugin-sonarjs`, `eslint-plugin-unicorn`, and `eslint-plugin-jest`

## v12.0.1 (2022-02-13)

### Fixed

- Disable rule `jest/prefer-snapshot-hint` due to inconsistent behavior (#51)

## v12.0.0 (2022-02-12)

### Added

- Added new configuration for [eslint-plugin-unicorn](https://www.npmjs.com/package/eslint-plugin-unicorn), which must be explicitly extended (i.e. `"extends": ["@aarongoldenthal/eslint-config-standard/unicorn-config"]`). (#142)
  - BREAKING: Disabled rules `no-nested-ternary` and `node/no-process-exit` in favor of `eslint-plugin-unicorn` replacements
  - Updated `ecmaVersion` to 2021 (for compatibility with `unicorn/numeric-separators-style`)

### Changed

- BREAKING: Added new `eslint-plugin-jest` rules [`no-conditional-in-test`](https://github.com/jest-community/eslint-plugin-jest/blob/main/docs/rules/no-conditional-in-test.md), [`prefer-comparison-matcher`](https://github.com/jest-community/eslint-plugin-jest/blob/main/docs/rules/prefer-comparison-matcher.md), [`prefer-equality-matcher`](https://github.com/jest-community/eslint-plugin-jest/blob/main/docs/rules/prefer-equality-matcher.md), and [`prefer-snapshot-hint`](https://github.com/jest-community/eslint-plugin-jest/blob/main/docs/rules/prefer-snapshot-hint.md). Removed deprecated rule `no-if` in favor of `no-conditional-in-test`. (#46, #48)
- BREAKING: Updated `eslint-plugin-node` configuration, adding many new rules. (#50)

### Fixed

- Updated to latest dependencies, including `eslint-plugin-jest` and `eslint-plugin-jsdoc`

## v11.0.0 (2021-12-31)

### Changed

- BREAKING: Upgraded `jest/recommended` rules that are warnings to be errors (`jest/expect-expect`, `jest/no-commented-out-tests`, `jest/no-disabled-tests`) (#40)
- BREAKING: Updated `max-lines-per-function` rule to ignore blank lines and comments. This is breaking since it could make existing disables unused, which is an error. (#43)
- BREAKING: Upgraded all `eslint-plugin-jsdoc/recommended` warnings to errors, and adding a few other errors. See [the config](./jsdoc-config.js) for complete details. (#38)

### Fixed

- Updated to latest dependencies, including `eslint-plugin-jest` and `eslint-plugin-jsdoc`

## v10.0.2 (2021-12-01)

### Fixed

- Updated to latest `eslint-plugin-sonarjs`, resolving all `eslint` v8 peer dependency issues (#42)
- Updated to latest dependencies, including `eslint-plugin-jest` and `eslint-plugin-jsdoc`

## v10.0.1 (2021-11-02)

### Fixed

- Updated to latest `eslint-plugin-jsdoc`, resolving node 17 engine error

## v10.0.0 (2021-10-14)

### Changed

- BREAKING: Upgraded to `eslint` v8 (#34)
- BREAKING: Upgraded to `eslint-plugin-jest` v25 (#35)
  - Renamed rules `lowercase-name` to `prefer-lowercase-title`, `valid-describe` to `valid-describe-callback`
  - Removed rules `prefer-to-be-undefined` and `prefer-to-be-null`
- BREAKING: Added `eslint-plugin-jest` v24 rules `max-nested-describe`, `prefer-expect-resolves`, and `prefer-to-be` as errors. (#33)
- Updated tests to have a dedicated test for loading config, code with no errors, and code with errors. (#36)

## v9.1.0 (2021-08-16)

### Changed

- Updated to `eslint` v7.32.0 and added test for `fatalErrorCount` (#29)

### Fixed

- Updated to latest dependencies, including `eslint-plugin-jest`, `eslint-plugin-jsdoc` and `eslint-plugin-sonarjs`

### Miscellaneous

- Integrate [renovate](https://docs.renovatebot.com/) for automatic dependency updates (#6)

## v9.0.0 (2021-07-11)

### Changed

- BREAKING: Added `eslint` rule `prefer-object-spread` as error (#25)
- BREAKING: Upgraded to `eslint-plugin-sonarjs` v0.9 and added new rules `elseif-without-else`, `no-empty-collection`, `no-gratuitous-expressions`, `no-nested-switch`, `no-nested-template-literals`, and `non-existent-operator` (#26)
- Updated `parserOptions` to ES2019, which is compatible in all currently supported versions of Node.js (#27)

### Fixed

- Updated to latest dependencies, including `eslint-plugin-jsdoc` and `eslint-plugin-sonarjs` and resolving vulnerabilities

## v8.0.0 (2021-05-31)

### Changed

- BREAKING: A number of rule settings in these configurations are identified as warnings. Many of these are fixable, so with modern IDEs they're functionally errors already. For others, experience has shown that it's easy for these to go unnoticed since they don't fail git hooks or CI linting jobs. So, upgraded `eslint`, `eslint-plugin-jest`, and `eslint-plugin-jsdoc` warnings to errors. (#23)
- BREAKING: Updated `space-before-function-paren` rule to require space after keywords and prevent after function name for consistency with `keyword-spacing` rule (#21)
- BREAKING: Deprecated support for Node 10 and 15 since end-of-life. Compatible with all current and LTS releases (`^12.20.0 || ^14.15.0 || >=16.0.0`). (#22)

### Fixed

- Updated to latest dependencies

## v7.0.2 (2021-05-09)

### Fixed

- Updated to latest dependencies, including resolving vulnerabilities and removing `husky`

## v7.0.1 (2021-04-11)

### Fixed

- Updated to latest dependencies, including `husky` v6

### Miscellaneous

- Optimize published package to only include the minimum required files (#18)

## v7.0.0 (2021-03-27)

### Changed

- BREAKING: Added rule `no-unsafe-optional-chaining` (from `eslint` v7.15.0) (#13)
- BREAKING: Updates rule `no-sequences` with `allowInParentheses=false` (from `eslint` v7.23.0) (#17)

### Fixed

- Updated to latest dependencies, including resolving vulnerabilities

## v6.2.0 (2021-03-13)

### Changed

- Moved `eslint-plugin-jest`, `eslint-plugin-jsdoc`, `eslint-plugin-node`, and `eslint-plugin-sonarjs` from `peerDependencies` to `dependencies` so they do not have to explicitly defined as dependencies in each project (#16)

### Fixed

- Updated to latest dependencies, including resolving vulnerabilities

### Miscellaneous

- Updated pipeline to use standard NPM package collection (#15)

## v6.1.4 (2021-02-21)

### Fixed

- Updated to latest dependencies

## v6.1.3 (2021-02-14)

### Fixed

- Reverted back to husky v4 after several issues (#14)

## v6.1.2 (2021-02-14)

### Fixed

- Updated to latest dependencies

## v6.1.1 (2021-01-03)

### Miscellaneous

- Updated CI pipeline to leverage simplified `include` syntax in GitLab 13.6 (#12)

### Fixed

- Updated to latest dependencies, including resolving vulnerabilities

## v6.1.0 (2020-11-22)

### Added

- Add configuration for [eslint-plugin-jsdoc](https://www.npmjs.com/package/eslint-plugin-jsdoc) (#11)

### Fixed

- Updated to latest dependencies

## v6.0.0 (2020-09-05)

### Changed

- BREAKING: Updated to [`eslint-plugin-jest`](https://github.com/jest-community/eslint-plugin-jest/releases/tag/v24.0.0) v24, which adds several new rules to `recommended`
- BREAKING: Added rules `no-promise-executor-return` and `no-unreachable-loop` (from `eslint` v7.3.0) (#10)
- BREAKING: Removed Node v13 compatibility since end-of-life. Compatible with all current and LTS releases (`^10.13.0 || ^12.13.0 || >=14.0.0`) (#9)

### Fixed

- Updated to latest dependencies, including resolving vulnerabilities

## v5.1.0 (2020-06-21)

### Changed

- Added `jest/prefer-expect-assertions` as warning

### Fixed

- Updated to latest dependencies

## v5.0.0 (2020-05-17)

### Changed

- BREAKING: Update rules for `eslint-plugin-jest` v23.12.0 to replace `no-truthy-falsy` with `no-restricted-matchers`

## v4.0.0 (2020-05-10)

### Changed

- BREAKING: Upgraded to `eslint` v7.0.0, including new `eslint:recommended` settings
- BREAKING: Update rules for `eslint` and `eslint-plugin-jest`
- Added `eslint-plugin-node` configuration since some core rules were move there in `eslint` v7.0.0

### Fixed

- Updated to latest dependencies
- Updated to latest GitLab `license_scanning` job

## v3.1.0 (2020-03-15)

### Changed

- Updated ECMAScript version to 2018 (#8)

### Fixed

- Updated to latest dependencies to resolve vulnerabilities

## v3.0.0 (2020-01-03)

### Changed

- BREAKING: Removed Node v8 compatibility since end-of-life. Compatible with all current and LTS releases (`^10.13.0 || ^12.13.0 || >=13.0.0`) (#6)

### Fixed

- Updated to latest dependencies

### Miscellaneous

- Moved project repository to new location at https://gitlab.com/gitlab-ci-utils/eslint-config-standard/ (#7)
- Documentation cleanup (#5)

## v2.1.0 (2019-12-02)

### Changed

- Add configuration for [eslint-plugin-sonarjs](https://www.npmjs.com/package/eslint-plugin-sonarjs) (#2)

### Miscellaneous

- Add dedicated tests for all supported NodeJS versions (#4)

## v2.0.0 (2019-11-24)

### Changed

- **Breaking**: Added new rules for Eslint 6.7.0 (#3)
- **Breaking**: Updated `camelcase` rule to show error for all

## v1.2.1 (2019-11-20)

### Fixed

- Updated to latest dependencies to resolve vulnerabilities

## v1.2.0 (2019-10-28)

### Changed

- Updated configuration for `eslint-plugin-jest` v23.0.0

## v1.1.0 (2019-10-27)

### Changed

- Added new rule for `eslint-plugin-jest` v22.21.0

## v1.0.1 (2019-10-26)

### Fixed

- Change module name to resolve npm-check issues (#1)

## v1.0.0 (2019-10-26)

Initial release
